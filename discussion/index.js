let posts = []; // storage
let count = 1; // id


// add post data.

document.querySelector("#form-add-post").addEventListener("submit", (e) => {

	e.preventDefault();

	posts.push({
		id: count,
		title: document.querySelector("#txt-title").value,
		body: document.querySelector("#txt-body").value
	});

	count++;

	//console.log(posts);
	// alert("Successfully added");
	showPosts(posts);
})

// Show Posts

const showPosts = (posts) => {
	let postEntries = "";

	posts.forEach((post) => {
		postEntries += `<div style="border-bottom: 1px solid red" id="post-${post.id}"> 
		<h3 id="post-title-${post.id}">${post.title} <h3/>
		<p id="post-body-${post.id}">${post.body}</p>
		<button onClick="editPost(${post.id})">Edit</button>
		<button onClick="deletePost(${post.id})">Delete</button>
		</div>`

	});
	document.querySelector("#div-post-entries").innerHTML = postEntries;
}

// Edit Post
const editPost = (id) => {
	// let post = posts.find(p => p.id == id)	
	// document.querySelector("#txt-edit-id").value = post.id
	// document.querySelector("#txt-edit-title").value = post.title
	// document.querySelector("#txt-edit-body").value = post.body

	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector("#txt-edit-id").value = id;
	document.querySelector("#txt-edit-title").value = title;
	document.querySelector("#txt-edit-body").value = body;
}

// Update Post

document.querySelector("#form-edit-post").addEventListener("submit", (e) => {

	e.preventDefault();
	for (let i = 0; i < posts.length; i++) {

		if (posts[i].id.toString() === document.querySelector("#txt-edit-id").value){
			posts[i].title = document.querySelector("#txt-edit-title").value
			posts[i].body = document.querySelector("#txt-edit-body").value
		}
	}
	showPosts(posts);
	alert ("Successfully updated")
})



const deletePost = (id) => {
	posts.forEach(p => {
	  for (let key in p) {
	  	if (p.id == id ){
			posts.splice(key, 1);
		}
		break;
	  }
	})	

	showPosts(posts);
}

